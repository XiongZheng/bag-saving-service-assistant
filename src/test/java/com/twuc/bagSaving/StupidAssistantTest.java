package com.twuc.bagSaving;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

class StupidAssistantTest extends SaveAndGetBagTest {

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_save_bag_when_get_client_bag(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        Ticket ticket = cabinet.save(savedBag, lockerSize);
        Bag fetchedBag = cabinet.getBag(ticket);
        assertSame(savedBag, fetchedBag);
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize"})
    void should_save_smaller_bag_to_small_locker(BagSize smallerBagSize, LockerSize smallLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Bag smallerBag = new Bag(smallerBagSize);
        Ticket ticket = cabinet.save(smallerBag, smallLockerSize);
        Bag receivedBag = cabinet.getBag(ticket);

        assertSame(smallerBag, receivedBag);
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize"})
    void should_save_middle_bag_to_middle_locker(BagSize middleBagSize, LockerSize middleLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Bag middleBag = new Bag(middleBagSize);
        Ticket ticket = cabinet.save(middleBag, middleLockerSize);
        Bag receivedBag = cabinet.getBag(ticket);

        assertSame(middleBag, receivedBag);
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize"})
    void should_save_bigger_bag_to_bigger_locker(BagSize biggerBagSize, LockerSize biggerLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Bag biggerBag = new Bag(biggerBagSize);
        Ticket ticket = cabinet.save(biggerBag, biggerLockerSize);
        Bag receivedBag = cabinet.getBag(ticket);

        assertSame(biggerBag, receivedBag);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneLockerSizeFull")
    void should_fail_save_bag_if_correspond_lockers_are_full(Cabinet fullCabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        InsufficientLockersException error = assertThrows(
                InsufficientLockersException.class,
                () -> fullCabinet.save(savedBag, lockerSize));

        assertEquals("Insufficient empty lockers.", error.getMessage());
    }
}
